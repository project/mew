<?php

/**
 * @file
 *   Contains translatable strings that aren't run through t() directly in the
 *   original code.
 */

/**
 * Return a list of interface strings.
 *
 * @return array
 *   An array with translated interface strings.
 */
function mew_potx() {
  return array(
    t('The title callback requires at least !count arguments.'),
    t('The page callback requires at least !count arguments.'),
    t('The access callback requires at least !count arguments.'),
    t('The theme callback requires at least !count arguments.'),
  );
}