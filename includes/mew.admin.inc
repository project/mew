<?php

/**
 * @file
 *   The Menu Wizard administration pages.
 */

/**
 * Save an overridden menu item.
 *
 * @param $item array
 *   The menu item to save.
 *
 * @return mixed
 *   The return value of drupal_write_record().
 */
function mew_save(array $item) {
  mew_delete($item['path']);
  foreach (array('title', 'page', 'access', 'theme') as $callback) {
    $item[$callback . '_arguments'] = serialize($item[$callback . '_arguments']);
  }

  return drupal_write_record('mew', $item);
}

/**
 * Delete an overwritten menu item.
 *
 * @param $path string
 *   The path of the menu item to delete.
 */
function mew_delete($path) {
  db_delete('mew')
    ->condition('path', $path)
    ->execute();
}

/**
 * List all available menu links.
 */
function mew_form_list(array $form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'mew') . '/css/mew.css');

  $form['keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter menu links on their URL path or title'),
    '#default_value' => isset($form_state['values']) ? $form_state['values']['keywords'] : NULL,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'mew') . '/js/mew.js'),
    ),
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#attributes' => array(
      'class' => array('js-hide'),
    ),
  );
  $keywords = array();
  if (isset($form_state['values']['keywords']) && drupal_strlen($form_state['values']['keywords'])) {
    $keywords = explode(' ', $form_state['values']['keywords']);
  }
  $form['list'] = array(
    '#markup' => mew_list($keywords),
    '#prefix' => '<div id ="mew-list-wrapper">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Form submit handler for mew_form_list().
 */
function mew_form_list_submit(array $form, array &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * List all available menu links.
 */
function mew_list(array $keywords = array()) {
  $items = mew_menu_items();
  $overwritten_paths = db_query("SELECT path FROM {mew}")->fetchCol();

  // Sort the items.
  ksort($items);
  if (isset($_GET['order']) && $_GET['order'] == t('Title')) {
    uasort($items, 'mew_sort_title');
  }
  elseif (isset($_GET['order']) && $_GET['order'] == t('Administrative')) {
    uasort($items, 'mew_sort_administrative');
  }
  if (isset($_GET['sort']) && $_GET['sort'] == 'desc') {
    $items = array_reverse($items);
  }

  $rows = array();
  foreach ($items as $path => $item) {
    if ($keywords) {
      $strings = array($path, $item['title']);
      $matches = array(TRUE, TRUE);
      foreach ($strings as $i => $string) {
        foreach ($keywords as $keyword) {
          if (stripos($string, $keyword) === FALSE) {
            $matches[$i] = FALSE;
          }
        }
      }
      // Not all keywords are present, so skip this menu link.
      if (!$matches[0] && !$matches[1]) {
        continue;
      }
    }

    $reset = $class = $administrative = NULL;
    if (in_array($path, $overwritten_paths)) {
      $class = 'mew-overwritten';
      $reset = l(t('reset'), 'admin/structure/mew/reset/' . $path);
    }
    if ($item['mew_administrative']) {
      $administrative = t('Yes');
    }
    $rows[] = array(
      'data' => array($path, $item['title'], mew_item_type($item), $administrative, l(t('edit'), 'admin/structure/mew/edit/' . $path), $reset),
      'class' => array($class),
    );
  }

  $header = array(
    array(
      'data' => t('URL path'),
      'field' => t('path'),
    ),
    array(
      'data' => t('Title'),
      'field' => t('title'),
    ),
    t('Type'),
    array(
      'data' => t('Administrative'),
      'field' => t('administrative'),
    ),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'mew-list'),
  ));
}

/**
 * Sort menu items by title. usort() callback.
 */
function mew_sort_title(array $item_a, array $item_b) {
  return strcmp($item_a['title'], $item_b['title']);
}

/**
 * Sort menu items by type. usort() callback.
 */
function mew_sort_type(array $item_a, array $item_b) {
  static $item_types = NULL;

  if (!$item_types) {
    $item_types = mew_list_types();
  }

  return strcmp($item_types[$item_a['type']], $item_types[$item_b['type']]);
}

/**
 * Sort menu items by their status as administrative path. usort() callback.
 */
function mew_sort_administrative(array $item_a, array $item_b) {
  if ($item_a['mew_administrative']) {
    return $item_b['mew_administrative'] ? 0 : -1;
  }
  else {
    return $item_b['mew_administrative'] ? 1 : 0;
  }
}

/**
 * Return a list of menu item types.
 *
 * @return array
 *   Keys are menu constants and values are human-readable type names.
 */
function mew_list_types() {
  return array(
    MENU_NORMAL_ITEM => t('Normal'),
    MENU_LOCAL_ACTION => t('Local action'),
    MENU_LOCAL_TASK => t('Tab (local task)'),
    MENU_DEFAULT_LOCAL_TASK => t('Default tab (local task)'),
    MENU_SUGGESTED_ITEM => t('Suggested'),
    MENU_CALLBACK => t('Hidden (callback)'),
    MENU_VISIBLE_IN_BREADCRUMB => t('Hidden (callback), but visible in breadcrumb'),
  );
}

/**
 * Create a human-readable type for a menu item.
 *
 * @param $item array
 *   The menu item.
 *
 * @return string
 *   The human-readable type of $item.
 */
function mew_item_type($item) {
  $list = mew_list_types();
  if ($item['type'] == MENU_CALLBACK) {
    return $list[MENU_CALLBACK];
  }
  else {
    unset($list[MENU_CALLBACK]);

    // Check item types.
    $types = array();
    foreach ($list as $type => $title) {
      if (($item['type'] & $type) == $type) {
        $types[$type] = $title;
      }
    }

    // Filter item types that are also part of another type.
    foreach (array_keys($types) as $type) {
      foreach (array_keys($types) as $type_reference) {
        if ($type != $type_reference) {
          if (($type_reference & $type) == $type) {
          unset($types[$type]);
        }
        }
      }
    }

    return implode(', ', $types);
  }
}

/**
 * Menu link edit form.
 */
function mew_form_edit(array $form, &$form_state) {
  // The first 25 characters are "admin/structure/mew/edit/", which is the menu
  // item for this form.
  $path = substr($_GET['q'], 25);
  if (!($item = mew_menu_item($path))) {
    drupal_not_found();
    exit;
  }

  drupal_set_title(t('Edit %url_path', array('%url_path' => $path)), PASS_THROUGH);

  $form_state['redirect'] = 'admin/structure/mew';
  $form['path'] = array(
    '#type' => 'value',
    '#value' => $path,
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Link type'),
    '#options' => mew_list_types(),
    '#default_value' => $item['type'],
  );
  $context = array();
  if ($item['context'] == (MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE)) {
    $context = array(MENU_CONTEXT_PAGE, MENU_CONTEXT_INLINE);
  }
  else {
    $context[] = $item['context'];
  }
  $form['context'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Contexts'),
    '#description' => t('Applies to tabs (local tasks) only.'),
    '#options' => array(
      MENU_CONTEXT_PAGE => t('Page'),
      MENU_CONTEXT_INLINE => t('Inline'),
    ),
    '#default_value' => $context,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'mew') . '/js/mew.js'),
    ),
  );
  $form['mew_administrative'] = array(
    '#type' => 'checkbox',
    '#title' => t('%path is an administrative page', array('%path' => $path)),
    '#default_value' => $item['mew_administrative'],
  );
  $form['file'] = array(
    '#type' => 'textfield',
    '#title' => t('Include file'),
    '#default_value' => $item['file'],
  );
  if (user_access('administer modules')) {
    $message = t('Entering incorrect function names or arguments may cause your website to malfunction. In case of trouble, <a href="@uninstall">uninstall Menu Wizard</a>.', array('@uninstall' => url('admin/modules', array('fragment' => 'edit-modules-other-mew-enable'))));
  }
  else {
    $message = t('Entering incorrect function names or arguments may cause your website to malfunction. In case of trouble, uninstall Menu Wizard.');
  }
  $callback_warning = '<div class="messages warning">' . $message . '</div>';
  $form['callbacks'] = array(
    '#type' => 'vertical_tabs',
  );  $callbacks = array(
    'title' => array(
      '#title' => t('Title callback'),
      '#callback_description' => t('Defaults to !t.', array('!t' => l('t()', 'http://api.drupal.org/api/function/t/7'))),
      '#arguments_description' => t("One argument per line. Integers will match the corresponding URL path component. The menu item's title will automatically be the first argument."),
    ),
    'page' => array(
      '#title' => t('Page callback'),
      '#callback_description' => NULL,
      '#arguments_description' => t('One argument per line. Integers will match the corresponding URL path component.'),
    ),
    'access' => array(
      '#title' => t('Access callback'),
      '#callback_description' => t('Enter <code>TRUE</code> to allow access to everyone.'),
      '#arguments_description' => t('One argument per line. Integers will match the corresponding URL path component.'),
    ),
    'theme' => array(
      '#title' => t('Theme callback'),
      '#callback_description' => t('Defaults to the main site theme.'),
      '#arguments_description' => t('One argument per line. Integers will match the corresponding URL path component.'),
    ),
    'delivery' => array(
      '#title' => t('Delivery callback'),
      '#callback_description' => t('Defaults to !drupal_deliver_html_page.', array('!drupal_deliver_html_page' => l('drupal_deliver_html_page()', 'http://api.drupal.org/api/function/drupal_deliver_html_page/7'))),
    ),
  );
  $form['callbacks'] = array(
    '#type' => 'vertical_tabs',
  );
  foreach ($callbacks as $callback => $callback_info) {
    $form[$callback] = array(
      '#type' => 'fieldset',
      '#title' => $callback_info['#title'],
      '#description' => $callback_warning,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'callbacks',
    );
    $form[$callback][$callback . '_callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback'),
      '#description' => $callback_info['#callback_description'],
      '#default_value' => $item[$callback . '_callback'],
    );
    if ($callback != 'delivery') {
      $disabled = FALSE;
      foreach ($item[$callback . '_arguments'] as $i => $argument) {
        if (is_object($argument) || is_array($argument)) {
          $item[$callback . '_arguments'][$i] = '#' . gettype($argument) . '#';
          $disabled = TRUE;
        }
      }
      $form[$callback][$callback . '_arguments'] = array(
        '#type' => 'textarea',
        '#title' => t('Arguments'),
        '#description' => $callback_info['#arguments_description'],
        '#default_value' => implode("\n", $item[$callback . '_arguments']),
        '#disabled' => $disabled,
      );
    }
  }
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $overridden = db_query("SELECT 1 FROM {mew} WHERE path = :path", array(
    ':path' => $path,
  ))->fetchField();
  if ($overridden) {
    $form['buttons']['reset'] = array(
      '#markup' => l(t('Reset'), 'admin/structure/mew/reset/' . $path),
    );
  }

  return $form;
}

/**
 * Submit callback for mew_form_edit().
 */
function mew_form_edit_validate(array $form, array &$form_state) {
  $values = $form_state['values'];

  // Confirm that the include file exists.
  if (!empty($values['file']) && !file_exists($values['file'])) {
    form_set_error('file', t('The include file does not exist.'));
  }
  // Confirm that callbacks exist.
  else {
    include($values['file']);
    $callbacks = array('title', 'page', 'access', 'theme', 'delivery');
    $error_messages = array(
      'title_callback' => t('The title callback cannot be found.'),
      'title_arguments' => 'The title callback requires at least !count arguments.',
      'page_callback' => t('The page callback cannot be found.'),
      'page_arguments' => 'The page callback requires at least !count arguments.',
      'access_callback' => t('The access callback cannot be found.'),
      'access_arguments' => 'The access callback requires at least !count arguments.',
      'theme_callback' => t('The theme callback cannot be found.'),
      'theme_arguments' => 'The theme callback requires at least !count arguments.',
      'delivery_callback' => t('The delivery callback cannot be found.'),
    );
    foreach ($callbacks as $callback) {
      if (isset($error_messages[$callback . '_callback'])) {
        if (!empty($values[$callback . '_callback'])) {
          if (!function_exists($values[$callback . '_callback'])) {
            form_set_error($callback . '_callback', $error_messages[$callback . '_callback']);
          }
          if (isset($error_messages[$callback . '_arguments'])) {
            $reflection = new ReflectionFunction($values[$callback . '_callback']);
            $count = $reflection->getNumberOfRequiredParameters();
            if ($callback == 'title') {
              $count--;
            }
            if (count(preg_split('#\v#', $values[$callback . '_arguments'], NULL, PREG_SPLIT_NO_EMPTY)) < $count) {
              form_set_error($callback . '_arguments', t($error_messages[$callback . '_arguments'], array('!count' => $count)));
            }
          }
        }
      }
    }
  }
}

/**
 * Submit callback for mew_form_edit().
 */
function mew_form_edit_submit(array $form, array &$form_state) {
  $item = $form_state['values'];
  // The context defaults to MENU_CONTEXT_NONE.
  switch (count($item['context'])) {
    case 0:
      $context = MENU_CONTEXT_NONE;
      break;
    case 1:
      $context = $item['context'][0];
      break;
    case 2:
      $context = MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE;
  }
  // The title callback defaults to t().
  if (empty($item['title_callback'])) {
    $item['title_callback'] = 't';
  }
  // The access callback can be TRUE to allow access to everyone.
  if ($item['access_callback'] == 'TRUE') {
    $item['access_callback'] = TRUE;
  }
  // The delivery callback defaults to drupal_deliver_html_page().
  if (empty($item['delivery_callback'])) {
    $item['delivery_callback'] = 'drupal_deliver_html_page';
  }
  // Convert callback arguments to arrays.
  foreach (array('title', 'page', 'access', 'theme') as $callback) {
    $item[$callback . '_arguments'] = preg_split('#\v#', $item[$callback . '_arguments'], NULL, PREG_SPLIT_NO_EMPTY);
    // Convert numbers to integers, so they match URL path components.
    foreach ($item[$callback . '_arguments'] as &$argument) {
      if (!preg_match('#\D#', $argument)) {
        $argument = (int) $argument;
      }
    }
  }
  mew_save($item);
  menu_rebuild();
  drupal_set_message(t('The menu link for %path has been overwritten.', array('%path' => $item['path'])));
}

/**
 * Form: reset a menu link.
 *
 * @return array
 *   A Drupal form.
 */
function mew_form_reset(array $form, array &$form_state) {
  // The first 26 characters are "admin/structure/mew/reset/", which is the menu
  // item for this form.
  $path = substr($_GET['q'], 26);

  if (!mew_menu_item($path)) {
    drupal_not_found();
    exit;
  }

  $form_state['redirect'] = 'admin/structure/mew';
  $form['path'] = array(
    '#type' => 'value',
    '#value' => $path,
  );

  return confirm_form($form,
    t('Are you sure you want to reset %title?', array('%title' => $path)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/structure/mew',
    t('This action cannot be undone.'),
    t('Reset'),
    t('Cancel')
  );
}

/**
 * Submit callback for mew_form_reset().
 */
function mew_form_reset_submit(array $form, array &$form_state) {
  mew_delete($form_state['values']['path']);
  menu_rebuild();
  drupal_set_message(t('The menu link for %path has been reset.', array('%path' => $form_state['values']['path'])));
}