(function ($) {
	/**
	 * Filter the menu links in the table at admin/build/mew.
	 */
	Drupal.behaviors.mewFilter = {
		attach: function (context, settings) {
			$('#mew-form-list').bind('submit', function(){
				return false;
			});
			$('#edit-keywords').bind('keyup', function() {
				var keywords = [];
				var fragments = this.value.split(' ');
				for (i in fragments) {
					keywords.push(fragments[i].toLowerCase());
				}
				var count = -1;
				// Clone the table body, so we can perform all manipulations behind the
				// scenes and the browser only needs to render the new table once.
				var table = $('#mew-list tbody');
				var table_clone = table.clone();
				table_clone.find('tr').each(function() {
					var cells = $(this).find('td');
					// Check the URL path and link title, which are the first two cells.
					var strings = [cells.get(0).innerHTML.toLowerCase(), cells.get(1).innerHTML.toLowerCase()];
					var matches = [true, true];
					for (i in strings) {
						// Check if all keywords are present in this string.
						for (j in keywords) {
							if (strings[i].indexOf(keywords[j]) == -1) {
								matches[i] = false;
							}
						}
					}
					// All keywords are present the path or title, so show the table row.
					if (matches[0] || matches[1]) {
						var row_class = count++ % 2 == 0 ? 'even' : 'odd';
						$(this).removeClass('even odd').addClass(row_class).show();
					}
					// Not all keywords are present, so hide the table row.
					else {
						$(this).hide();
					}
				});
				table.replaceWith(table_clone);
			});
		}
	}

	/**
	 * Recalculate context visibility based on link type.
	 */
	Drupal.behaviors.mewContext = {
		attach: function (context, settings) {
			var type = $('#mew-form-edit #edit-type').val();
			var context = $('#mew-form-edit .form-item-context');
			context.find('.description').hide();
			if (type == 132 || type == 140) {
				context.show();
			}
			else {
				context.hide();
			}
			$('#mew-form-edit #edit-type').bind('change', function() {
				if (this.value == 132 || this.value == 140) {
					context.show(500);
				}
				else {
					context.hide(500);
				}
			});
		}
	}

	/**
	* Vertical tabls summaries for callback information.
	*/
	Drupal.behaviors.mewCallbackSummaries = {
		attach: function (context, settings) {
			var callbacks = ['title', 'page', 'access', 'theme', 'delivery'];
			for (i in callbacks) {
		    $('fieldset#edit-' + callbacks[i], context).drupalSetSummary(function (context) {
					var callback = context.id.substr(5);
					var arguments_processed = [];
					if ($('#edit-' + callback + '-arguments').val() != undefined) {
						var arguments = $('#edit-' + callback + '-arguments').val().split(/\r|\n/);
						for (j in arguments) {
							if (arguments[j].length) {
								if (arguments[j].match('\\D')) {
									arguments_processed.push("'" + arguments[j].replace("'", "\\'") + "'");
								}
								else {
									arguments_processed.push(arguments[j]);
								}
							}
						}
					}
		     	return $('#edit-' + callback + '-callback').val() + '(' + arguments_processed.join(', ') + ')';
	    	});
			}
		}
	}
})(jQuery);